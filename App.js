import React from "react";
import { NavigationContainer } from "@react-navigation/native";
import { createNativeStackNavigator } from "@react-navigation/native-stack";

import Home from "./screens/app/Home";
import NewsDetails from "./components/NewsComponent/NewsDetails";
import NewsList from "./components/NewsComponent/NewsList";
import EventList from "./components/EventComponent/EventList";
import EventDetails from "./components/EventComponent/EventDetails";
import ForgotPassword from "./screens/LoginForms/ForgotPassword";
import Login from "./screens/LoginForms/Login";

import Register from "./screens/LoginForms/Register";
import Checker from "./screens/LoginForms/Checker";
import ProfileDetials from "./screens/Profile/ProfileDetails";
import UserDrawer from "./screens/user/screens/UserHome";
import User_EventDetails from "./screens/user/UserEventContainer/user_EventDetails";
import User_EventList from "./screens/user/UserEventContainer/User_EventList";
import SplashScreen from "./screens/app/LandingScreen";
import { LogBox } from 'react-native';
require('./rnfbLogLevel');


LogBox.ignoreAllLogs(); // Ignore all warning logs

const stack = createNativeStackNavigator();

export default function App() {
  return (
    <NavigationContainer>
      <stack.Navigator
        screenOptions={{
          headerShown: false,
        }}
      >
        <stack.Screen
          name="Splash"
          component={SplashScreen}
          options={{ headerShown: false }}
        />
        <stack.Screen name="Login" component={Login} />
        <stack.Screen name="Home" component={Home} />
        <stack.Screen name="UserHome" component={UserDrawer} />
        <stack.Screen name="NewsDetails" component={NewsDetails} />
        <stack.Screen name="NewsList" component={NewsList} />
        <stack.Screen name="User_EventDetails" component={User_EventDetails} />
        <stack.Screen name="User_EventList" component={User_EventList} />
        <stack.Screen
          name="ProfileDetials"
          component={ProfileDetials}
          options={{ headerShown: false }}
        />
        <stack.Screen
          name="EventDetails"
          component={EventDetails}
          options={{
            headerShown: false,
          }}
        />
        <stack.Screen name="EventList" component={EventList} />
        <stack.Screen name="ForgotPassword" component={ForgotPassword} />

        <stack.Screen name="Register" component={Register} />

        <stack.Screen name="Checker" component={Checker} />
      </stack.Navigator>
    </NavigationContainer>
  );
}
