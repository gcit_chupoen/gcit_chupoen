
import {  Alert } from "react-native";
import { doc,deleteDoc} from "firebase/firestore";
import { db } from "../../Firebase/firebase";

const ConfirmDeletePost = async (itemId) => {
  console.log("itemid",itemId)

  try{
    deleteDoc(doc(db, "categories", itemId));
    // console.log('deleted')
  } catch (error){
    console.log(error)
  }

  // const posts = doc(db, "categories", itemId);
  // await updateDoc(posts, {
  //   capital: deleteField(),
  // });
};

const DeletePost = (itemId) =>

  Alert.alert("Are You Sure?", "Are you Sure you want to delete this post", [
    {
      text: "Cancel",
      onPress: () => console.log(itemId),
    },
    { text: "OK", onPress: () => ConfirmDeletePost(itemId) },
  ]);

export default DeletePost;
