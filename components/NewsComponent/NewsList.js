import React from "react";
import { Pressable} from "react-native";
import NewsCard from "./NewsCard";
import { useNavigation } from "@react-navigation/native";

export default function NewsList({ NewsDetails }) {
  const navigation = useNavigation();

  return (
    <Pressable onPress={() => navigation.navigate('NewsDetails', { NewsDetails})}>
      <NewsCard NewsDetails={NewsDetails} />
    </Pressable>
  );
}
