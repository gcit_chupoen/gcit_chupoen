import React from "react";
import { View, StyleSheet, useWindowDimensions, Text } from "react-native";
import { ActivityIndicator } from "react-native-paper";

const Loader = ({ visiable = false }) => {
  const { height, width } = useWindowDimensions();
  return (
    visiable && (
      <View style={[styles.container, { height, width }]}>
        <View style={styles.LoaderContainer}>
          <View>
            <ActivityIndicator size="large" color={"#FFA500"} />
          </View>
          <View style={styles.LoaderTextContainer}>
            <Text style={styles.LoaderText}>Signing Up....</Text>
          </View>
        </View>
      </View>
    )
  );
};

const styles = StyleSheet.create({
  container: {
    position: "absolute",
    zIndex: 10,
    backgroundColor: "rgba(0,0,0,0.5)",
    justifyContent: "center",
  },
  LoaderContainer: {
    height: 100,
    backgroundColor: "white",
    marginHorizontal: 50,
    borderRadius: 5,
    flexDirection: "row",
    alignItems: "center",
    paddingHorizontal: 20,
  },
  LoaderText: {
    marginRight: -20,
    fontSize: 17,
    color:"grey"
  },
  LoaderTextContainer:{
    // borderWidth:2,
    marginLeft:10,
  }
});
export default Loader;
