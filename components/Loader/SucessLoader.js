import React from "react";
import { useNavigation } from "@react-navigation/native";
import {
  View,
  StyleSheet,
  useWindowDimensions,
  Text,
  TouchableOpacity,
} from "react-native";

const SucessLoader = ({ visiable = false }) => {
  const { height, width } = useWindowDimensions();

  const navigation = useNavigation();
  const buttonHandler = () => {
    navigation.replace("Login");
  };

  return (
    visiable && (
      <View style={[styles.container, { height, width }]}>
        <View style={styles.LoaderContainer}>
          <View style={styles.LoaderTextContainer}>
            <Text style={styles.LoaderText}>Sucessfully Registered</Text>
          </View>
          <View style={styles.LoaderTextContainer2}>
            <TouchableOpacity onPress={buttonHandler}>
              <Text style={styles.LoaderText2}>OK</Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    )
  );
};

const styles = StyleSheet.create({
  container: {
    position: "absolute",
    zIndex: 10,
    backgroundColor: "rgba(0,0,0,0.5)",
    justifyContent: "center",
  },
  LoaderContainer: {
    display: "flex",
    flexDirection: "column",
    height: 170,
    backgroundColor: "white",
    marginHorizontal: 50,
    borderRadius: 5,
    alignItems: "center",
    paddingHorizontal: 20,
    justifyContent: "center",
  },
  LoaderText: {
    fontSize: 20,  
    fontWeight:"bold"
  },
  LoaderText2: {
    fontSize: 20,
    color: "#77a8f7",

   
  },
  LoaderTextContainer: {
    width: "100%",
    paddingBottom: 10,
    marginTop: 30,
    marginLeft:40,
    // borderWidth: 2,
  },
  LoaderTextContainer2: {
    width: "100%",
    alignItems: "center",
    marginTop: 20,
    borderTopWidth:2,
    height:50,
    padding:10,
    borderTopColor:"#e1e4e8"
  },
});
export default SucessLoader;
