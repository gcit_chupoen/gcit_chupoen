import React, { useState } from "react";
import { Pressable} from "react-native";
import EventCard from "./EventCard";
import { useNavigation } from "@react-navigation/native";

export default function EventList({ EventDetails }) {
  const navigation = useNavigation();
  return (
    <Pressable onPress={() => navigation.navigate('EventDetails', { EventDetails})}>
      <EventCard EventDetails={EventDetails} />
    </Pressable>
  );
}
