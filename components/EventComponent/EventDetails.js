import {
  View,
  Text,
  ImageBackground,
  StyleSheet,
  TouchableOpacity,
  FlatList,
  ScrollView,
} from "react-native";
import { Ionicons } from "@expo/vector-icons";

export default function EventDetails({ route, navigation }) {
  const Details = route.params?.EventDetails;

  // console.log(Details);

  return (
    <View style={styles.mainContianer}>
      <View style={styles.ImageContainer}>
        <ImageBackground
          source={{ uri: Details?.Picture }}
          borderRadius={10}
          style={styles.images}
        >
          <View style={styles.child} />
        </ImageBackground>

        <View style={styles.BackButton}>
          <TouchableOpacity onPress={() => navigation.goBack()}>
            <Ionicons
              name="ios-chevron-back-outline"
              size={30}
              color="#F9AE00"
            />
          </TouchableOpacity>
        </View>

        <View style={styles.Title}>

          <Text style={styles.TitleText} > {Details.Title.length > 150
            ? `${Details.Title.substring(0, 150)}....`
            : Details.Title}</Text>
        </View>

        <View style={styles.PostedDate}>
          <Text style={styles.PostedDateText}>{Details?.Date}</Text>
        </View>
      </View>

      <View style={styles.MainTitle}>
        <Text style={styles.MainTitleText}>{Details?.Title}</Text>
      </View>
      <ScrollView style={styles.descriptionContainer} >

          <Text style={styles.descriptionText} > {Details?.Description}</Text>
  
      </ScrollView>
    </View>
  );
}

const styles = StyleSheet.create({
  mainContianer: {
    flex: 1,
    backgroundColor: "white",

    // backgroundColor: "#F7F7F7",
  },
  ImageContainer: {
    height: "30%",
    width: "100%",
    // paddingLeft:5,
    // paddingRight:5,
  },
  images: {
    height: "100%",
    width: "100%",

    backgroundColor: "transparent",
  },
  BackButton: {
    marginTop: -225,
    // borderWidth: 2,
    width: 40,
    marginLeft: 10,
  },
  child: {
    flex: 1,
    backgroundColor: "rgba(0,0,0,0.4)",
    borderBottomLeftRadius: 10,
    borderBottomRightRadius: 10,
  },
  PostedDate: {
    // marginTop: 10,
    marginLeft: 20,
  },
  PostedDateText: {
    color: "white",
    fontWeight: "bold",
  },
  Title: {
    marginTop: 100,
    marginLeft: 20,
    paddingBottom: 10,
  },
  TitleText: {
    fontSize: 17,
    fontWeight: "bold",
    color: "white",
  },
  MainTitle: {
    // borderWidth: 2,
    borderLeftWidth: 3,
    borderColor: "#F9AE00",
    width: "60%",
    marginLeft: 40,
    marginTop: 20,
    paddingLeft: 10,
    backgroundColor: "white",
  },
  MainTitleText: {
    fontSize: 20,
    fontWeight: "bold",
  },
  descriptionContainer: {
    marginTop: 20,
  
    width: "80%",
  
    marginLeft: 40,
  },
  descriptionText: {
    fontSize: 16,
  },
});
