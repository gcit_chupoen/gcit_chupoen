import React from "react";
import { StyleSheet, View, Text } from "react-native";
import { Linking } from "react-native";

function QuickLinks() {
  const openLink = (url) => {
    Linking.openURL(url);
  };

  return (
    <View style={styles.quickLinksContainer}>
      <View style={styles.innerContainer}>
        <View style={styles.header}>
          <Text style={styles.headerText}>E-SERVICES</Text>
        </View>
        <View style={styles.links}>
          <Text
            style={styles.linkText}
            onPress={() =>
              openLink(
                "https://docs.google.com/forms/d/e/1FAIpQLScp9HCeB1xBpg6QjlxpmvGSZiebPPM2RTanxEmU7fR4WtmNIQ/viewform"
              )
            }
          >
            ICT Help Desk
          </Text>
          <Text
            style={styles.linkText}
            onPress={() => openLink("https://vle.gcit.edu.bt/login/index.php")}
          >
            Virtual Learning Environment-Moodle
          </Text>
          <Text
            style={styles.linkText}
            onPress={() =>
              openLink("https://ims.rub.edu.bt/public/auth/view-login")
            }
          >
            RUB-Information Management System
          </Text>
          <Text
            style={styles.linkText}
            onPress={() => openLink("http://library.gcit.edu.bt/")}
          >
            Libarary Catalog(OPAC)-koha
          </Text>
          <Text
            style={styles.linkText}
            onPress={() =>
              openLink(
                "https://accounts.google.com/v3/signin/identifier?dsh=S1560998242%3A1685529551995519&btmpl=mobile&continue=https%3A%2F%2Fmail.google.com%2Fmail%2F&hd=rub.edu.bt&ifkv=Af_xneECulN8MriLBDNluD6cYBZe4vHK-yCcbU7UsbE-pMY7g1171YJ2aIh1EqqJH2Iu-wWtp4ijLg&ltmpl=ecobx&osid=1&rip=1&sacu=1&service=mail&flowName=GlifWebSignIn&flowEntry=ServiceLogin"
              )
            }
          >
            RUB Web Mail
          </Text>
          <Text
            style={styles.linkText}
            onPress={() =>
              openLink(
                "https://accounts.google.com/v3/signin/identifier?dsh=S17577936%3A1685529718015147&continue=https%3A%2F%2Fdocs.google.com%2Fforms%2Fd%2Fe%2F1FAIpQLSeGbhg_tQPdsmpUYNsZpr_9VjTLRD9vNof8pETOjiPqhvDRyg%2Fviewform&followup=https%3A%2F%2Fdocs.google.com%2Fforms%2Fd%2Fe%2F1FAIpQLSeGbhg_tQPdsmpUYNsZpr_9VjTLRD9vNof8pETOjiPqhvDRyg%2Fviewform&ifkv=Af_xneF4KofNO5SnOognvhIZ9KTQLVSQQfEfpz8QEL8XsQSOCl9oWOf0GxVgBFg2_cP2XI-B_ehV&ltmpl=forms&osid=1&passive=1209600&service=wise&flowName=GlifWebSignIn&flowEntry=ServiceLogin"
              )
            }
          >
            Staff Residence Application Form
          </Text>
          <Text
            style={styles.linkText}
            onPress={() => openLink("https://gcit.emcwub.pce.edu.bt/")}
          >
            Happiness and Wellbeing Center
          </Text>

          <Text
            style={styles.linkText}
            onPress={() =>
              openLink(
                "https://docs.google.com/forms/d/1wKenT38X4syoJBFPMY58qVtGtNsdHBzVpNUrsdC4Gz0/viewform?edit_requested=true"
              )
            }
          >
            Estate Help Desk
          </Text>
        </View>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  quickLinksContainer: {
    flex: 1,
    padding: 20,
    paddingTop: 40,
    backgroundColor: "#f5f5f5",
  },
  innerContainer: {
    borderWidth: 1,
    borderColor: "grey",
    borderRadius: 10,
    padding: 10,
    backgroundColor: "#ffffff",
    height: "100%",
  },
  header: {
    marginBottom: 10,
  },
  headerText: {
    fontSize: 20,
    marginLeft: 5,
    color: "#FF7F18",
    fontWeight: "500",
  },
  linkText: {
    color: "#007BFF",
    textDecorationLine: "underline",
    fontWeight: "500",
    fontSize: 15,
    marginBottom: 10,
  },
  links: {
    // borderWidth:1,
    marginLeft: 6,
  },
});

export default QuickLinks;
