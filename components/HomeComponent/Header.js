import React, { useEffect, useState } from "react";
import { Image, View, StyleSheet, Dimensions,ImageBackground,Text } from "react-native";
import Carousel from "react-native-snap-carousel";
import { collection, getDocs } from "firebase/firestore";
import { db } from "../../Firebase/firebase";

const { height } = Dimensions.get("window");

function Header() {
  const [data, setData] = useState([]);

  useEffect(() => {
    const fetchData = async () => {
      try {
        const querySnapshot = await getDocs(collection(db, "categories"));
        const documents = querySnapshot.docs.map((doc) => {
          // console.log(doc.id, " => ", doc.data());
          return doc.data();
        });
        setData(documents);
      } catch (error) {
        console.log(error);
      }
    };

    fetchData();
  }, []);

  const renderCarouselItem = ({ item }) => {
    return (
      <>
        <View style={styles.main}>
          <View >
            <ImageBackground
              borderRadius={10}
              style={styles.image} source={{ uri: item.Picture }}
             
            >
              <View style={styles.child} />
            </ImageBackground>
          </View>
        </View>
        <View>
        <Text style={styles.Titletext2}>{item.Date}</Text>
        <Text style={styles.Titletext}>
          {item.Title.length > 50
            ? `${item.Title.substring(0, 50)}....`
            : item.Title}
        </Text>
      </View>
      
      </>
    );
  };

  return (
    <View style={styles.rootContainer}>
      <Carousel
        data={data}
        renderItem={renderCarouselItem}
        sliderWidth={Dimensions.get("window").width}
        itemWidth={Dimensions.get("window").width}
        autoplay={true}
        autoplayInterval={3000}
        loop={true}
      />
    </View>
  );
}

export default Header;

const styles = StyleSheet.create({
  rootContainer: {
    width: "100%",
    height: height / 4,
    borderRadius: 10,
    overflow: "hidden",
  },
  image: {
    width: "100%",
    height: "100%",
    borderRadius: 10,
    overflow: "hidden",
  },
  imageContainer: {
    paddingHorizontal: 15,
    backgroundColor: "transparent",
  },
  child: {
    flex: 1,
    backgroundColor: "rgba(0,0,0,0.7)",
    borderRadius: 10,
  },
  Titletext2: {
    color: "white",
    fontSize: 13,
    fontWeight: "bold",

    height: 40,
    marginTop: -50,
    marginLeft: 20,
  },
  Titletext: {
    color: "white",
    fontSize: 17,
    fontWeight: "bold",
    marginTop: -80,
    marginLeft: 20,
  },
});
