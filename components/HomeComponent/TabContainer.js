import React from "react";
import { Tab, TabView } from "@rneui/themed";
import { StyleSheet } from "react-native";

import News from "../NewsComponent/NewsContainer";
import Events from "../EventComponent/EventContainer";
import QuickLinks from "../HomeComponent/QuickLinks";

export default () => {
  const [index, setIndex] = React.useState(0);

  return (
    <>
      <Tab style={styles.Category} value={index} onChange={(e) => setIndex(e)}>
        <Tab.Item
          containerStyle={(active) => ({
            backgroundColor: active ? "white" : "#F9AE00",
            height:active ? 55: 50,
          })}
          title="Events"
          titleStyle={(active) => ({
            fontWeight: "bold",
            fontSize: 17,
            color: active ? "#FF7F18" : "white",
          })}
        />
        <Tab.Item
          containerStyle={(active) => ({
            backgroundColor: active ? "white" : "#F9AE00",
            height:active ? 55: 50,
          })}
          title="News"
          titleStyle={(active) => ({
            fontWeight: "bold",
            fontSize: 17,
            color: active ? "#FF7F18" : "white",
          })}
        />

        <Tab.Item
          containerStyle={(active) => ({
            backgroundColor: active ? "white" : "#F9AE00",
            height:active ? 55: 50,
          })}
          title="QuickLink"
          titleStyle={(active) => ({
            fontWeight: "bold",
            fontSize: 17,
            color: active ? "#FF7F18" : "white",
          })}
        />
      </Tab>

      <TabView value={index} onChange={setIndex} animationType="spring">
        <TabView.Item style={{ backgroundColor: "white", width: "100%" }}>
          <Events />
        </TabView.Item>
        <TabView.Item style={{ backgroundColor: "white", width: "100%" }}>
          <News />
        </TabView.Item>
        <TabView.Item style={{ backgroundColor: "white", width: "100%" }}>
          <QuickLinks />
        </TabView.Item>
      </TabView>
    </>
  );
};

const styles = StyleSheet.create({
  Category: {
    borderTopWidth: 1,
    borderTopColor: "white",
    height: 55,
    backgroundColor: "#F9AE00",
    margin: 10,
  },
});
