const originalConsoleWarn = console.warn;
console.warn = (...args) => {
  const ignoredWarnings = [
    // Add warning messages that you want to ignore
    'Setting a timer',
    'Warning: componentWillReceiveProps',
  ];

  const isIgnoredWarning = ignoredWarnings.some(ignoredWarning => args[0].includes(ignoredWarning));

  if (!isIgnoredWarning) {
    originalConsoleWarn.apply(console, args);
  }
};
