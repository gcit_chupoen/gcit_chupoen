import React, { useEffect, useRef } from "react";
import { View, Image, StyleSheet, Animated } from "react-native";
import { SplashScreen as LoaderScreen } from "../app/LandingScreen";
import Checker from "../LoginForms/Checker";

export const SplashScreen = ({ navigation }) => {
  const logoAnimation = useRef(new Animated.Value(0)).current;

  useEffect(() => {
    const timer = setTimeout(() => {
      navigation.replace("Checker");
    }, 3000);

    return () => clearTimeout(timer);
  }, [navigation]);

  useEffect(() => {
    startLogoAnimation();
  }, []);

  const startLogoAnimation = () => {
    Animated.timing(logoAnimation, {
      toValue: 1,
      duration: 1000,
      useNativeDriver: true,
    }).start();
  };

  console.log("splash");

  return (
    <View style={styles.container}>
      <Animated.Image
        source={require("../../assets/image/logo.png")}
        style={[
          styles.logo,
          {
            transform: [
              {
                scale: logoAnimation.interpolate({
                  inputRange: [0, 1],
                  outputRange: [0.5, 1],
                }),
              },
            ],
          },
        ]}
        resizeMode="contain"
        onError={() => console.log("Error loading splash logo")}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#FFFFFF",
    alignItems: "center",
    justifyContent: "center",
  },
  logo: {
    width: 200,
    height: 200,
    resizeMode: "contain",
  },
});

export default SplashScreen;
