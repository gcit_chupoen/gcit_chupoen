import * as React from "react";
import { View, StyleSheet, Text } from "react-native";
import Header from "../../components/HomeComponent/Header";
import { doc, getDoc } from "firebase/firestore";
import { db } from "../../Firebase/firebase";
import { useState } from "react";

import TabItems from "../../components/HomeComponent/TabContainer";
import { Entypo } from "@expo/vector-icons";

import { MaterialIcons } from "@expo/vector-icons";
import { MaterialCommunityIcons } from "@expo/vector-icons";
import PostForm from "../post/post";
import { StatusBar } from "expo-status-bar";
import { useNavigation, useScrollToTop } from "@react-navigation/native";

import { useEffect } from "react";
import AsyncStorage from "@react-native-async-storage/async-storage";

import ProfileScreen from "../Profile/profileScreen";

import {
  createDrawerNavigator,
  DrawerContentScrollView,
  DrawerItemList,
  DrawerItem,
} from "@react-navigation/drawer";

function HomeHandler({ navigation }) {
  return (
    <>
      <View>
        <Header />

        <View style={styles.menuButton}>
          <Entypo
            name="menu"
            size={30}
            color="white"
            onPress={() => navigation.openDrawer()}
          />
        </View>

        <View
          style={{
            height: "75%",

            marginTop: 146,
          }}
        >
          <TabItems />
        </View>
      </View>
    </>
  );
}

function Post() {
  return (
    <View style={{ height: "100%", padding: 20 }}>
      <PostForm />
    </View>
  );
}
function Profile() {
  return (
    <View style={{ height: "100%", paddingTop: 40, backgroundColor: "black" }}>
      <ProfileScreen />
    </View>
  );
}

function CustomDrawerContent(props) {
  const navigation = useNavigation();
  const [loggedInUser, setLoggedInUser] = useState(null);
  const [data, setData] = useState([]);

  const getData = async () => {
    const email = await AsyncStorage.getItem("Email");
    return email;
  };

  useEffect(() => {
    const fetchData = async () => {
      setLoggedInUser(await getData());
    };
    fetchData();
  }, []);

  useEffect(() => {
    const ReadData = async () => {
      const docRef = doc(db, "users", loggedInUser);
      const docSnap = await getDoc(docRef);
      const itemArray = [];

      if (docSnap.exists()) {
        itemArray.push({
          name: docSnap.data().Name,
          year: docSnap.data().Year,
          semester: docSnap.data().Semester,
        });
        setData(itemArray);
      }
    };

    ReadData();
  }, [loggedInUser, data]);
  const logout = async () => {
    await AsyncStorage.removeItem("Email")
      .then(() => {
        console.log("loggedIn");
      })
      .catch((error) => {
        console.log(error.message);
      });
    await AsyncStorage.removeItem("pass")
      .then(() => {
        console.log("OK");
      })
      .catch((error) => {
        console.log(error.message);
      });
    navigation.replace("Login");
  };

  return (
    <>
      <StatusBar style="light" />
      <View style={styles.DrawerContainer}>
        <View style={styles.ProfilePhoto}>
          <View style={styles.initialContainer}>
            <Text style={styles.initialText}>
              {data[0]?.name ? data[0]?.name.charAt(0).toUpperCase() : ""}
            </Text>
          </View>
          <Text style={styles.Pname}>{data[0]?.name}</Text>
        </View>
       

        <View style={{ height: "100%" }}>
          <DrawerContentScrollView {...props}>
            <DrawerItemList {...props} />
            <DrawerItem
              label="LogOut"
              onPress={logout}
              icon={() => (
                <MaterialCommunityIcons
                  name="logout"
                  size={24}
                  color="#F9AE00"
                />
              )}
            />
          </DrawerContentScrollView>
        </View>
      </View>
    </>
  );
}

const Drawer = createDrawerNavigator();

export default function MyDrawer() {
  useEffect(() => {
    getData();
  }, []);
  const getData = async () => {
    const email = await AsyncStorage.getItem("Email");
  };
  return (
    <>
      <Drawer.Navigator
        drawerContent={(props) => <CustomDrawerContent {...props} />}
        drawerContentOptions={{
          drawerPosition: "Right",
        }}
      >
        <Drawer.Screen
          name="Home"
          component={HomeHandler}
          options={{
            headerShown: false,
            drawerIcon: () => <Entypo name="home" size={24} color="#F9AE00" />,
          }}
        />
        <Drawer.Screen
          name="Post"
          component={Post}
          options={{
            headerStyle: {
              backgroundColor: "#F9AE00",
            },
            headerTintColor: "#FFFFFF",

            drawerIcon: () => (
              <MaterialIcons name="description" size={24} color="#F9AE00" />
            ),
          }}
        />
        <Drawer.Screen
          name="Profile"
          component={Profile}
          options={{
            drawerIcon: () => (
              <MaterialCommunityIcons
                name="account"
                size={24}
                color="#F9AE00"
              />
            ),
            headerShown: false,
          }}
        />
      </Drawer.Navigator>
    </>
  );
}

const styles = StyleSheet.create({
  menuButton: {
    width: 40,
    marginTop: -180,
    marginLeft: 360,
  },
  DrawerContainer: {
    height: "100%",
  },

  ProfilePhoto: {
    flexDirection: "row",
    alignItems: "center",
    backgroundColor: "#F9AE00",
    height: "18%",
  },
  initialContainer: {
    width: 70,
    height: 70,
    borderRadius: 35,
    backgroundColor: "#f0f0f0",
    justifyContent: "center",
    alignItems: "center",
    marginLeft: 10,
  },
  initialText: {
    fontSize: 28,
    fontWeight: "bold",
    color: "#333",
  },
  Pname: {
    marginLeft: 10,
    fontSize: 18,
    fontWeight: "400",
    color: "white",
  },
});
