import AsyncStorage from "@react-native-async-storage/async-storage";
import { useNavigation } from "@react-navigation/native";
import{ useEffect, useState } from "react";


const Checker = () => {
  const navigation = useNavigation();
  const [isLoading, setIsLoading] = useState(true);

  const CheckLogin = async () => {
    try {
      const email = await AsyncStorage.getItem("Email");
      const pass = await AsyncStorage.getItem("Password");
      const adminEmail = "123@gmail.com"; // Replace with the admin's email

      if (email && pass) {
        if (email === adminEmail) {
          console.log('email home', email);
          navigation.reset({
            index: 0,
            routes: [{ name: 'Home' }],
          });
        } else {
          console.log('email new screen', email);
          navigation.reset({
            index: 0,
            routes: [{ name: 'UserHome' }],
          });
        }
      } else {
        navigation.replace("Login");
        console.log('email login', email);
      }
    } catch (error) {
      console.error("AsyncStorage error:", error);
      navigation.replace("Login");
    } finally {
      setIsLoading(false);
    }
  };

  useEffect(() => {
    CheckLogin();
  }, []);
};

export default Checker;
