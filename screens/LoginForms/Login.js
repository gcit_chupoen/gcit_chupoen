import React, { useState } from "react";
import {
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  Image,
  SafeAreaView,
  Dimensions,
  TextInput,
  ImageBackground,
} from "react-native";

import { useNavigation } from "@react-navigation/native";
import { Entypo } from "@expo/vector-icons";
import { auth } from "../../Firebase/firebase";

import { signInWithEmailAndPassword, getAuth } from "firebase/auth";

import AsyncStorage from "@react-native-async-storage/async-storage";

const { width } = Dimensions.get("window");
const inputWidth = width * 0.6;
const { height } = Dimensions.get("window");
const inputHeight = height * 0.06;
const font = width * 0.06;

const Login = () => {
  const navigation = useNavigation();
  const [email, setEmail] = useState(null);
  const [password, setPassword] = useState(null);

  const saveEmailPass = async () => {
    try {
      await AsyncStorage.setItem("Email", email);
      await AsyncStorage.setItem("Password", password);
    } catch (e) {
      console.log(e);
    }
  };

  const [isPasswordVisible, setIsPasswordVisible] = useState(false);

  const togglePasswordVisibility = () => {
    setIsPasswordVisible(!isPasswordVisible);
  };

  const handleLogin = async () => {
    try {
      const userCredentials = await signInWithEmailAndPassword(
        auth,
        email,
        password
      );
      const user = userCredentials.user;
      console.log("logged in with:", user.email);
      await saveEmailPass();

      const adminEmail = "123@gmail.com"; // Replace with the admin's email
      if (user.email === adminEmail) {
        navigation.replace("Home");
      } else {
        navigation.replace("UserHome"); // Replace with the screen for non-admin users
      }
    } catch (error) {
      const errorCode = error.code;
      const errorMessage = error.message;

      switch (errorCode) {
        case "auth/user-not-found":
          alert("User not found. Please check your email and try again.");
          break;
        case "auth/wrong-password":
          alert("Incorrect password. Please try again.");
          break;
        default:
          alert(errorMessage);
          break;
      }
    }
  };

  const handleTextPress = () => {
    navigation.navigate("ForgotPassword");
  };

  const handleRegister = () => {
    navigation.navigate("Register");
  };

  return (
    <SafeAreaView style={styles.container}>
      <View style={styles.imageConatiner}>
        <ImageBackground
          style={styles.image2}
          source={require("../../assets/image/pattern.png")}
        >
          <Image
            style={styles.image}
            source={require("../../assets/image/logo.png")}
          />
        </ImageBackground>
      </View>
      <View style={styles.innerContainer}>
        <View style={styles.loginCredentialsConatianer}>
          <TextInput
            style={styles.input}
            placeholder="Email"
            value={email}
            keyboardType="email-address"
            onChangeText={(text) => setEmail(text)}
          />
          <TextInput
            style={styles.input}
            placeholder="Password"
            secureTextEntry={!isPasswordVisible}
            value={password}
            onChangeText={setPassword}
          />
          <TouchableOpacity
            style={styles.toggleButton}
            onPress={togglePasswordVisibility}
          >
            {isPasswordVisible ? (
              <Entypo name="eye" size={20} color="#F79515" style={styles.eye} />
            ) : (
              <Entypo
                name="eye-with-line"
                size={20}
                color="#FFA500"
                style={styles.eye}
              />
            )}
          </TouchableOpacity>

          <View style={styles.text}>
            <Text
              onPress={handleTextPress}
              style={[
                { color: "#F79515", marginLeft: -140, paddingBottom: 30 },
              ]}
            >
              Forgot Password?
            </Text>
            {/* <Text style={[styles.Text, { color: "#FFA500" }]}>Login as Admin</Text> */}
          </View>
          <TouchableOpacity style={styles.button} onPress={handleLogin}>
            <Text style={styles.buttonText}>Sign in</Text>
          </TouchableOpacity>
        </View>
      </View>
      <View style={styles.ORimageContainer}>
        <Image
          style={styles.ORimage}
          source={require("../../assets/image/OR.png")}
        />
      </View>
      <View style={[styles.text, { marginTop: "10%" }]}>
        <Text style={{ color: "black" }}>Don't have an account?</Text>
        <Text onPress={handleRegister} style={{ color: "#F79515" }}>
          Register
        </Text>
      </View>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
    backgroundColor: "white",
  },
  imageConatiner: {
    height: "40%",
    width: "100%",
  },
  loginCredentialsConatianer: {
    width: inputWidth + 95,

    alignItems: "center",
    // borderWidth:1,
  },

  title: {
    fontSize: 20,
    textAlign: "center",
    margin: 10,
  },
  input: {
    width: inputWidth + 50,
    height: inputHeight,
    borderWidth: 1,
    borderColor: "#D3D3D3",
    borderRadius: 10,
    paddingHorizontal: 10,
    marginVertical: 10,
  },
  button: {
    backgroundColor: "#FFD662",
    borderRadius: 10,
    padding: 10,
    width: inputWidth + 50,
  },
  buttonText: {
    color: "black",
    fontSize: 17,
    fontWeight: "bold",
    textAlign: "center",
  },
  text: {
    flexDirection: "row",
  },

  image: {
    width: inputWidth,
    height: inputHeight + 100,
  },
  ORimageContainer: {
    // borderWidth:2,
    padding: 10,
    width: inputWidth + 150,
    height: inputHeight,
    alignItems: "center",
  },

  image2: {
    width: "100%",
    height: "100%",
    // borderWidth: 2,
    justifyContent: "center",
    alignItems: "center",
  },
  eye: {
    marginTop: -50,
    marginLeft: 190,
  },
  innerContainer: {
    // elevation: 3,
    paddingTop: 30,
    height: inputHeight + 270,
    marginTop: -80,
    elevation: 4,
  },
  ORimage: {
    height: "100%",
    width: "80%",
  },
});

export default Login;
