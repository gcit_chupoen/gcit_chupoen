import React, { useState } from "react";
// import { Configapp, firebase } from "../../Firebase/firebase";
import { auth } from "../../Firebase/firebase";
import { Formik } from "formik";
import * as Yup from "yup";
import Loader from "../../components/Loader/loader";
import SucessLoader from "../../components/Loader/SucessLoader";
import { setDoc, doc} from "firebase/firestore";
import { db } from "../../Firebase/firebase";

import {
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  View,
  Image,
  ImageBackground,
  SafeAreaView,
  Dimensions,
  Alert,
} from "react-native";
import { Entypo } from "@expo/vector-icons";

const { width } = Dimensions.get("window");
const inputWidth = width * 0.7;
const { height } = Dimensions.get("window");
const inputHeight = height * 0.06;
const HeightWidth = width * 1;
const flexwidth = width * 0.91;

const SignupSchema = Yup.object().shape({
  email: Yup.string().email("Please use RUB mail").required("*Required"),
  password: Yup.string()
    .min(8)
    .required("Please enter your password.")
    .matches(
      "^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,}$",
      " Must Contain minum 8 character \n -Atleast one uppercase letter \n -One number \n -One special character"
    ),
  confirmPassword: Yup.string()
    .min(8, "Confirm your password")
    .oneOf([Yup.ref("password")], "Your Passwords do not match.")
    .required("Confirm password is required"),
});

const Register = ({ navigation }) => {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const inputs = [email, password];

  const [loading, setLoading] = useState(false);
  const [sucessLoading, setSucessLoading] = useState(false);

  const [isPasswordVisible, setIsPasswordVisible] = useState(false);
  const [isConfirmPasswordVisible, setIsConfirmPasswordVisible] =
    useState(false);

  const togglePasswordVisibility = () => {
    setIsPasswordVisible(!isPasswordVisible);
  };

  const toggleConfirmPasswordVisibility = () => {
    setIsConfirmPasswordVisible(!isConfirmPasswordVisible);
  };

  const userDetails=async()=>{
    try{
      const docRef = await setDoc(doc(db, "users",email), {
        Name:"Unknown",
        Year:"None",
        Gender:"None",
        Email:email,
        Semester:"None"
        
        
      });

    }catch(error){
      console.log(error.message)
    }
  }

  const createUser = () => {
    setLoading(true);
    setTimeout(() => {
      setLoading(false);
      try {
        // AsyncStorage.setItem("user", JSON.stringify(inputs));
        auth
          .createUserWithEmailAndPassword(email, password)
          .then((userCredentials) => {
            const user = userCredentials.user;
            userDetails();

            setSucessLoading(true);
          })
          .catch((error) => {
            Alert.alert("Error", "Invalid Email/Email Already In use");
          });
      } catch (error) {
        Alert.alert("Error", "User already exist");
      }
    }, 3000);
  };

  return (
    <Formik
      initialValues={{
        email: "",
        password: "",
        confirmPassword: "",
      }}
      validationSchema={SignupSchema}
    >
      {({
        values,
        errors,
        touched,
        handleSubmit,
        handleChange,
        setFieldTouched,
        handleBlur,
        isValid,
      }) => (
        <SafeAreaView style={styles.container}>
          <Loader visiable={loading} />
          <SucessLoader visiable={sucessLoading} />
          <View style={styles.imageContainer}>
            <ImageBackground
              style={styles.image2}
              source={require("../../assets/image/pattern.png")}
            >
              <Text style={styles.RegisterText}>Register</Text>
              <Text style={styles.RegisterText2}>Create your new account</Text>
            </ImageBackground>
          </View>
          <View style={styles.downArea}>
            <View>
              <View>
                <Text
                  style={{ color: "grey", opacity: 0.7, fontWeight: "bold" }}
                >
                  YOUR EMAIL
                </Text>
                <TextInput
                  style={styles.input}
                  placeholder="example.gcit@rub.edu.bt"
                  placeholderTextColor="rgba(153, 153, 153, 0.5)"
                  value={`${values.email}`}
                  onChangeText={(text) => {
                    handleChange("email")(text);
                    setEmail(text);
                  }}
                  onBlur={() => setFieldTouched("email")}
                />
                {touched.email && errors.email && (
                  <Text style={styles.ErrorTextStyle}>{errors.email}</Text>
                )}
              </View>

              <View>
                <View style={styles.containerPassword}>
                  <TextInput
                    style={styles.input}
                    placeholder="Password"
                    placeholderTextColor="rgba(153, 153, 153, 0.5)"
                    secureTextEntry={!isPasswordVisible}
                    value={`${values.password}`}
                    onChangeText={(text) => {
                      handleChange("password")(text);
                      setPassword(text);
                    }}
                    onBlur={() => setFieldTouched("password")}
                  />
                  <TouchableOpacity
                    style={styles.toggleButton}
                    onPress={togglePasswordVisibility}
                  >
                    {isPasswordVisible ? (
                      <Entypo
                        name="eye"
                        size={20}
                        color="#FFA500"
                        style={styles.eye}
                      />
                    ) : (
                      <Entypo
                        name="eye-with-line"
                        size={20}
                        color="#FFA500"
                        style={styles.eye}
                      />
                    )}
                  </TouchableOpacity>
                </View>
                {touched.password && errors.password && (
                  <Text style={styles.ErrorTextStyle2}>{errors.password}</Text>
                )}
              </View>

              <View>
                <View style={styles.containerPassword}>
                  <TextInput
                    style={styles.input}
                    placeholder="confirmPassword"
                    placeholderTextColor="rgba(153, 153, 153, 0.5)"
                    secureTextEntry={!isConfirmPasswordVisible}
                    value={values.confirmPassword}
                    onChangeText={handleChange("confirmPassword")}
                    onBlur={() => setFieldTouched("confirmPassword")}
                  />
                  <TouchableOpacity
                    style={styles.toggleButton}
                    onPress={toggleConfirmPasswordVisibility}
                  >
                    {isConfirmPasswordVisible ? (
                      <Entypo
                        name="eye"
                        size={20}
                        color="#FFA500"
                        style={styles.eye}
                      />
                    ) : (
                      <Entypo
                        name="eye-with-line"
                        size={20}
                        color="#FFA500"
                        style={styles.eye}
                      />
                    )}
                  </TouchableOpacity>
                </View>
                {touched.confirmPassword && errors.confirmPassword && (
                  <Text style={styles.ErrorTextStyle2}>
                    {errors.confirmPassword}
                  </Text>
                )}
              </View>

              <TouchableOpacity
                disabled={!isValid}
                style={[
                  styles.button,
                  { backgroundColor: isValid ? "#FFD662" : "#ab8746" },
                ]}
                onPress={createUser}
              >
                <Text style={styles.buttonText}>Sign Up</Text>
              </TouchableOpacity>
            </View>
            <View style={styles.ORimageContainer}>
              <Image
                style={styles.ORimage}
                source={require("../../assets/image/OR.png")}
              />
            </View>
            <View style={[styles.text, { marginTop: "10%" }]}>
              <Text style={{ color: "black" }}>Don't have an account?</Text>
              <Text style={{ color: "#F79515" }}>Login</Text>
            </View>
          </View>
        </SafeAreaView>
      )}
    </Formik>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "flex-end",
    alignItems: "center",
    backgroundColor: "white",
  },
  containerPassword: {
    justifyContent: "center",
    alignItems: "center",
  },

  imageContainer: {
    width: HeightWidth,
    height: HeightWidth + 200,
    flex: 1.5,
  },
  RegisterText: {
    fontWeight: "bold",
    fontSize: 35,
    color: "#F79515",
    opacity: 0.7,
    marginTop: 70,
  },
  RegisterText2: {
    fontSize: 15,
    color: "#F79515",
    opacity: 0.7,
  },
  downArea: {
    flex: 3,
    alignItems: "center",
    height: 100,
  },
  button: {
    borderRadius: 10,
    padding: 10,
    width: inputWidth,
    marginTop: 30,
  },
  buttonText: {
    color: "black",
    fontSize: 17,
    fontWeight: "bold",
    textAlign: "center",
  },
  input: {
    width: inputWidth,
    height: inputHeight,
    borderWidth: 1,
    borderColor: "#D3D3D3",
    borderRadius: 10,
    paddingHorizontal: 10,
    marginVertical: 10,
  },
  eye: {
    marginTop: -50,
    marginLeft: 190,
    opacity: 0.7,
  },
  ErrorTextStyle: {
    color: "red",
    fontSize: 10,
  },
  ErrorTextStyle2: {
    color: "red",
    fontSize: 10,
    width: inputWidth,
  },
  image2: {
    width: "100%",
    height: "105%",
    borderWidth:1,

    justifyContent: "center",
    alignItems: "center",
  },
  ORimage: {
    height: "100%",
    width: "80%",
  },
  ORimageContainer: {
    padding: 10,
    width: inputWidth + 150,
    height: inputHeight,
    alignItems: "center",
  },
  text: {
    flexDirection: "row",
  },
});

export default Register;
