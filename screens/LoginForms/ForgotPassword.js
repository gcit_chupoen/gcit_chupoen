import React, { useState } from "react";
import {
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  View,
  Image,
  SafeAreaView,
  Dimensions,
  Alert,
  ImageBackground,
} from "react-native";
import { getAuth, sendPasswordResetEmail } from "firebase/auth";

const { width } = Dimensions.get("window");
const inputWidth = width * 0.7;
const { height } = Dimensions.get("window");
const inputHeight = height * 0.06;
const HeightWidth = width * 1;
const flexwidth = width * 0.91;
const imageWidth = width * 0.2;
const imageHeight = height * 0.1;

const ForgotPassword = ({ navigation }) => {
  const [Email, setEmail] = useState("");

  const handleForgotPassword = async () => {
    try {
      if (Email === "") {
        Alert.alert("Please enter your email");
        return;
      }

      const auth = getAuth();
      sendPasswordResetEmail(auth, Email)
        .then(() => {
          Alert.alert(
            "Password Reset Email Sent",
            "Please check your email to reset your password.",
            [
              {
                text: "OK",
                onPress: () => navigation.navigate("Login"),
                style: "default",
              },
            ],
            { cancelable: false }
          );
        })
        .catch((error) => {
          if (error.code === "auth/user-not-found") {
            Alert.alert(
              "User Not Found",
              "Please check your email and try again.",
              [
                {
                  text: "OK",
                  style: "default",
                },
              ],
              { cancelable: false }
            );
          } else {
            Alert.alert(
              "Error",
              error.message,
              [
                {
                  text: "OK",
                  style: "default",
                },
              ],
              { cancelable: false }
            );
          }
        });
    } catch (err) {
      Alert.alert("Error", err.message);
    }
  };

  return (
    <SafeAreaView style={styles.container}>
      <View style={styles.mainContainer}>
        <ImageBackground
          style={styles.image2}
          source={require("../../assets/image/pattern.png")}
        >
          <Image
            style={styles.imagelogo}
            source={require("../../assets/image/logo.png")}
          />
        </ImageBackground>
      </View>
      <View style={styles.downArea}>
        <View style={{ marginTop: "5%", marginBottom: "3%" }}>
          <Text
            style={{
              fontWeight: "400",
              fontSize: 20,
              color: "#F79515",
            }}
          >
            Password Recovery
          </Text>
        </View>

        <View>
          <Image
            style={styles.image}
            source={require("../../assets/image/message.png")}
          />
        </View>

        <View>
          <Text style={{ color: "#D3D3D3" }}>
            Enter the registered Email for recovery of Password
          </Text>
        </View>

        <View>
          <TextInput
            style={styles.input}
            placeholder="Email"
            value={Email}
            onChangeText={(text) => setEmail(text)}
          />
          <TouchableOpacity
            style={styles.button}
            onPress={handleForgotPassword}
          >
            <Text style={styles.buttonText}>Submit</Text>
          </TouchableOpacity>
        </View>

        <View style={styles.dotArea}>
          <View style={styles.dotDisplay}></View>
          <View style={styles.dot}></View>
          <View style={styles.dot}></View>
        </View>
      </View>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#FFFFFF",
  },
  mainContainer: {
    width: "100%",
    height: "30%",
  },

  downArea: {
    alignItems: "center",
    height: "100%",
    backgroundColor: "#FFFFFF",
  },
  button: {
    marginTop: 30,
    backgroundColor: "#FFD662",
    borderRadius: 10,
    padding: 10,
    width: inputWidth + 50,
  },
  buttonText: {
    color: "#FFFFFF",
    fontSize: 16,
    fontWeight: "bold",
    textAlign: "center",
  },
  input: {
    width: inputWidth + 50,
    height: inputHeight,
    borderWidth: 1,
    borderColor: "#D3D3D3",
    borderRadius: 10,
    paddingHorizontal: 10,
    marginVertical: 10,
  },
  image: {
    width: imageWidth,
    height: imageHeight,
  },
  image2: {
    width: "100%",
    height: "100%",
  },
  imagelogo: {
    width: 250,
    height: 200,
    marginLeft: 90,
  },
});

export default ForgotPassword;
