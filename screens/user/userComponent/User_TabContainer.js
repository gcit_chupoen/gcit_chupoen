import React from "react";
import { Tab, TabView } from "@rneui/themed";
import { StyleSheet } from "react-native";

import User_EventsContainer from "../UserEventContainer/user_EventContainer";
import User_QuickLinks from "./user_QuickLinks";
import User_NewsContainer from "../user_NewsComponent/User_NewsContainer";

export default () => {
  const [index, setIndex] = React.useState(0);

  return (
    <>
      <Tab style={styles.Category} value={index} onChange={(e) => setIndex(e)}>
        <Tab.Item
          containerStyle={(active) => ({
            backgroundColor: active ? "white" : "#F9AE00",
            height:active ? 55: 50,
          })}
          title="Events"
          titleStyle={(active) => ({
            fontWeight: "bold",
            fontSize: 17,
            color: active ? "#FF7F18" : "white",
          })}
        />
        <Tab.Item
          containerStyle={(active) => ({
            backgroundColor: active ? "white" : "#F9AE00",
            height:active ? 55: 50,
          })}
          title="News"
          titleStyle={(active) => ({
            fontWeight: "bold",
            fontSize: 17,
            color: active ? "#FF7F18" : "white",
          })}
        />

        <Tab.Item
          containerStyle={(active) => ({
            backgroundColor: active ? "white" : "#F9AE00",
            height:active ? 55: 50,
          })}
          title="QuickLink"
          titleStyle={(active) => ({
            fontWeight: "bold",
            fontSize: 17,
            color: active ? "#FF7F18" : "white",
          })}
        />
      </Tab>

      <TabView value={index} onChange={setIndex} animationType="spring">
        <TabView.Item style={{ backgroundColor: "white", width: "100%" }}>
          <User_EventsContainer />
        </TabView.Item>
        <TabView.Item style={{ backgroundColor: "white", width: "100%" }}>
        <User_NewsContainer />
        </TabView.Item>
        <TabView.Item style={{ backgroundColor: "white", width: "100%" }}>
          <User_QuickLinks />
        </TabView.Item>
      </TabView>
    </>
  );
};

const styles = StyleSheet.create({
  Category: {
    borderTopWidth: 1,
    borderTopColor: "white",
    height: 55,
    backgroundColor: "#F9AE00",
    margin: 10,
  },
});
