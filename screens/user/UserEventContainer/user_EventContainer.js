import React, { useEffect, useState } from "react";
import { View, FlatList } from "react-native";
import { collection, getDocs } from "firebase/firestore";
import { db } from "../../../Firebase/firebase";
import User_EventList from "./User_EventList";
export default function User_EventsContainer() {
  const [data, setData] = useState([]);

  useEffect(() => {
    const fetchData = async () => {
      const querySnapshot = await getDocs(collection(db, "categories"));

      const itemArray = [];
      querySnapshot.forEach((doc) => {
        const { Date, Description, Picture, Selected_Value, Title } =
          doc.data();
        if (doc.data().Selected_Value === "Events") {
          itemArray.push({
            id: doc.id,
            Date,
            Description,
            Picture,
            Selected_Value,
            Title,
          });
        }
      });
      itemArray.sort((a, b) => b.Date.localeCompare(a.Date));
      setData(itemArray);
    };
    fetchData();
  }, [data]);

  return (
    <View>
      <FlatList
        data={data}
        renderItem={({ item }) => <User_EventList EventDetails={item} />}
        keyExtractor={(item) => item.id}
        showsVerticalScrollIndicator={false}
      />
    </View>
  );
}
