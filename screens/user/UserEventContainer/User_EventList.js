import React, { useState } from "react";
import { Pressable} from "react-native";
import User_EventCard from "./user_EventsCards";
import { useNavigation } from "@react-navigation/native";

export default function User_EventList({ EventDetails }) {
  const navigation = useNavigation();
  return (
    <Pressable onPress={() => navigation.navigate('EventDetails', { EventDetails})}>
      <User_EventCard EventDetails={EventDetails} />
    </Pressable>
  );
}
