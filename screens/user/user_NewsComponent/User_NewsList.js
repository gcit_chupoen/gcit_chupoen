import React from "react";
import { Pressable} from "react-native";
import User_NewsCard from "./User_NewsCard";
import { useNavigation } from "@react-navigation/native";

export default function User_NewsList({ NewsDetails }) {
  const navigation = useNavigation();

  return (
    <Pressable onPress={() => navigation.navigate('NewsDetails', { NewsDetails})}>
      <User_NewsCard NewsDetails={NewsDetails} />
    </Pressable>
  );
}
