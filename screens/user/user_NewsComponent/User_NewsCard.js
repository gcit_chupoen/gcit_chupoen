import {
  View,
  Text,
  StyleSheet,
  Dimensions,
  ImageBackground,
  TouchableOpacity,
} from "react-native";

import { useNavigation } from "@react-navigation/native";

const { width } = Dimensions.get("window");

export default function User_NewsCard({ NewsDetails }) {
  const navigation = useNavigation();
  return (
    <View style={styles.rootContainer}>
      <View style={styles.main}>
        <View style={styles.Images}>
          <ImageBackground
            borderRadius={10}
            source={{
              uri: NewsDetails.Picture
                ? NewsDetails.Picture
                : "https://img.freepik.com/free-photo/comfy-wing-chair-carpenter-s-workshop_181624-25989.jpg?w=996&t=st=1680757716~exp=1680758316~hmac=d9d982317ba3912c07d467c35d6b64a5829bfac8e68fe4e8f662d9f65f224cec",
            }}
            style={styles.imageContainer}
          >
            <View style={styles.child} />
          </ImageBackground>
        </View>
      </View>
      <View>
        <Text style={styles.Titletext2}>{NewsDetails.Date}</Text>
        <Text style={styles.Titletext}>
          {NewsDetails.Title.length > 20
            ? `${NewsDetails.Title.substring(0, 60)}....`
            : NewsDetails.Title}
        </Text>
      </View>
      <View style={styles.ReadMore}>
        <TouchableOpacity
          onPress={() => navigation.navigate("User_NewsDetails", { NewsDetails })}
        >
          <Text style={styles.Titletext3}>ReadMore</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  rootContainer: {
    width: "80%",
    height: width / 2.5 + 50,
    borderRadius: 10,
    marginTop: 20,
    marginBottom: 20,
    marginLeft: 50,
    elevation: 8,
    // backgroundColor: "rgba(0,0,0,0.1)",
    backgroundColor: "white",
  },

  imageContainer: {
    height: width / 2,
    backgroundColor: "transparent",
    borderRadius: 10,
  },

  Images: {
    width: "100%",
    borderRadius: 20,
    paddingTop: 10,
    borderRadius: 10,
  },

  main: {
    flexDirection: "row",
  },
  Titletext: {
    color: "white",
    fontSize: 17,
    fontWeight: "bold",

    marginTop: -80,
    marginLeft: 20,
  },
  Titletext2: {
    color: "white",
    fontSize: 13,
    fontWeight: "bold",

    height: 40,
    marginTop: -50,
    marginLeft: 20,
  },
  Titletext3: {
    color: "white",
  },
  DeleteButton: {
    height: 23,
    width: 25,
    marginTop: -170,
    marginLeft: 280,
  },
  child: {
    flex: 1,
    backgroundColor: "rgba(0,0,0,0.5)",
    borderRadius: 10,
  },
  ReadMore: {
    alignItems: "center",
    backgroundColor: "#FF7F18",
    height: 30,
    width: 100,
    marginTop: -50,
    marginLeft: 210,
    paddingTop: 5,
    elevation: 8,
    borderRadius: 10,
  },
});
