import React, { useEffect, useState } from "react";
import { Text, View, FlatList } from "react-native";

import { db } from "../../../Firebase/firebase";
import { collection, getDocs } from "firebase/firestore";
import User_NewsList from "./User_NewsList";


export default function User_NewsContainer() {
  const [data, setData] = useState([]);
  useEffect(() => {
    const fetchData = async () => {
      const querySnapshot = await getDocs(collection(db, "categories"));
      const itemArray = [];
      querySnapshot.forEach((doc) => {
        const { Date, Description, Picture, Selected_Value, Title } =
          doc.data();
        if (doc.data().Selected_Value === "News") {
          itemArray.push({
            id: doc.id,
            Date,
            Description,
            Picture,
            Selected_Value,
            Title,
          });
        }
      });
      itemArray.sort((a, b) => b.Date.localeCompare(a.Date));

      setData(itemArray);
    
    };
    fetchData();
  }, [data]);

  return (
    <View>
      <FlatList
        data={data}
        renderItem={({ item }) => <User_NewsList NewsDetails={item} />}
        keyExtractor={(item) => item.id}
        showsVerticalScrollIndicator={false}
      />
    </View>
  );
}
