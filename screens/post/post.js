import React, { useEffect, useState } from "react";
import { Stack, TextInput } from "@react-native-material/core";

import {
  KeyboardAvoidingView,
  View,
  Image,
  Text,
  StyleSheet,
  Pressable,
  Platform,
  ScrollView,
  TouchableOpacity,
  ActivityIndicator,
} from "react-native";
import {  useNavigation } from "@react-navigation/native";

import * as ImagePicker from "expo-image-picker";
import DateTimePicker from "@react-native-community/datetimepicker";
import { db, storage } from "../../Firebase/firebase";
import { ref, uploadBytesResumable, getDownloadURL } from "firebase/storage";
import { addDoc, collection } from "firebase/firestore";
import DropDownPicker from "react-native-dropdown-picker";

const Post = () => {
  const navigation = useNavigation();
  const [image, setImage] = useState(null);
  const [title, setTitle] = useState("");
  const [picture, setPicture] = useState(null);
  const [postDate, setPostdate] = useState("");

  const [selectedValue, setSelectedValue] = useState(null);
  const [open, setOpen] = useState(false);

  const [description, setDescription] = useState(null);
  const [date, setDate] = useState(new Date());
  const [showPicker, setShowPicker] = useState(false);
  const [uploading, setUploading] = useState(false); // Track upload status

  const options = [
    { label: "News", value: "News" },
    { label: "Events", value: "Events" },
  ];

  const handleDropdownChange = (value) => {
    setSelectedValue(value);
  };

  const toggleDatepicker = () => {
    setShowPicker(!showPicker);
  };

  const onDateTextChange = ({ type }, selectedDate) => {
    if (type === "set") {
      const currentDate = selectedDate || date;
      setDate(currentDate);
      if (Platform.OS === "android") {
        toggleDatepicker();
      }
      const formattedDate = formatDate(currentDate);
      setPostdate(formattedDate);
    } else {
      toggleDatepicker();
    }
  };

  const formatDate = (rawDate) => {
    let date = new Date(rawDate);
    let year = date.getFullYear();
    let month = date.getMonth() + 1;
    let day = date.getDate();
    return `${month}/${day}/${year}`;
  };

  const pickImage = async () => {
    let result = await ImagePicker.launchImageLibraryAsync({
      mediaTypes: ImagePicker.MediaTypeOptions.Images,
      //   allowsEditing: true,
      //   aspect: [4, 3],
      //   quality: 1,
    });

    console.log(result);

    if (!result.canceled) {
      setImage(result.assets[0].uri);
    }
  };

  const uploadImage = async () => {
    //convert Image into blob image
    const blobImage = await new Promise((resolve, reject) => {
      const xhr = new XMLHttpRequest();
      xhr.onload = function () {
        resolve(xhr.response);
      };
      xhr.onerror = function () {
        reject(new TypeError("Network request failed"));
      };
      xhr.responseType = "blob";
      xhr.open("GET", image, true);
      xhr.send(null);
    });

    //set metadata of image
    const metadata = {
      contentType: "image/jpeg",
    };

    //upload image on storage
    const storageRef = ref(storage, "Categories/" + Date.now());
    const uploadTask = uploadBytesResumable(storageRef, blobImage, metadata);

    // Listen for state changes, errors, and completion of the upload.
    uploadTask.on(
      "state_changed",
      (snapshot) => {
        // Get task progress, including the number of bytes uploaded and the total number of bytes to be uploaded
        const progress =
          (snapshot.bytesTransferred / snapshot.totalBytes) * 100;
        console.log("Upload is " + progress + "% done");
        switch (snapshot.state) {
          case "paused":
            console.log("Upload is paused");
            break;
          case "running":
            console.log("Upload is running");
            break;
        }
      },
      (error) => {
        switch (error.code) {
          case "storage/unauthorized":
            console.log("User doesn't have permission to access the object");
            break;
          case "storage/canceled":
            console.log("User canceled the upload");
            break;
          case "storage/unknown":
            console.log("Unknown error occurred, inspect error.serverResponse");
            break;
        }
      },
      () => {
        // Upload completed successfully, now we can get the download URL
        getDownloadURL(uploadTask.snapshot.ref).then((downloadURL) => {
          console.log("File available at", downloadURL);
          setPicture(downloadURL);
        });
      }
    );
  };

  const uploadCategory = async () => {
    await uploadImage();
    if (picture && title && selectedValue && description && postDate) {
      try {
        setUploading(true); // Set the uploading state to true
        const formattedDate = date.toISOString().split("T")[0]; // Extract the date part and remove the time component
        const docRef = await addDoc(collection(db, "categories"), {
          Picture: picture,
          Title: title,
          Selected_Value: selectedValue,
          Description: description,
          Date: formattedDate, // Use the formatted date string without the time
        });
        alert("uploaded");
        setDescription(null);
        setSelectedValue(null);
        setTitle(null);
        setDate(null);
        navigation.replace("Home");
      } catch (error) {
        console.log(error);
        // Handle error here
      } finally {
        setUploading(false); // Set the uploading state to false after completion (success or error)
      }
    }
  };
  const Cancelbutton = () => {
    setDescription(null);
    setSelectedValue(null);
    setTitle(null);
    setDate(new Date());
    setPostdate("");
    setImage(null);
  };
  return (
    <>
      <KeyboardAvoidingView
        behavior={Platform.OS === "ios" ? "padding" : "height"}
        style={{ flex: 1 }}
      >
        <View
          style={{
            height: "95%",
            backgroundColor: "white",
            borderRadius: 20,
            marginTop: 20,
          }}
        >
          <Stack spacing={10} style={{ margin: 16 }}>
            <View style={{ zIndex: 1 }}>
              <DropDownPicker
                items={options}
                defaultValue={selectedValue}
                placeholder="Select an Category"
                containerStyle={{ height: 60 }}
                style={{
                  backgroundColor: "#fafafa",
                  borderColor: "grey",
                  height: 60,
                }}
                dropDownStyle={{ backgroundColor: "#fafafa" }}
                open={open}
                setOpen={setOpen}
                value={selectedValue}
                setValue={setSelectedValue}
                onChangeValue={handleDropdownChange}
              />
            </View>

            <TextInput
              label="Title"
              variant="outlined"
              value={title}
              onChangeText={(text) => setTitle(text)}
            />
            <View>
              {showPicker && (
                <DateTimePicker
                  mode="date"
                  display="spinner"
                  value={date}
                  onChange={onDateTextChange}
                />
              )}
              {!showPicker && (
                <Pressable onPress={toggleDatepicker}>
                  <TextInput
                    label="Date"
                    variant="outlined"
                    value={postDate}
                    onChangeText={(text) => setDate(text)}
                    editable={false}
                  />
                </Pressable>
              )}
            </View>

            <ScrollView>
              <View
                style={{
                  height: "95%",
                  backgroundColor: "white",
                  borderRadius: 20,
                  marginTop: 20,
                }}
              >
                <View style={{ maxHeight: 200 }}>
                  <TextInput
                    label="Description"
                    variant="outlined"
                    value={description}
                    onChangeText={(text) => setDescription(text)}
                    textAlignVertical="top"
                    multiline={true}
                    numberOfLines={9}
                    style={{ height: "100%" }}
                  />
                </View>
              </View>
            </ScrollView>
            <View style={styles.buttonContainer}>
              <View style={{ width: "40%" }}>
                <TouchableOpacity style={styles.button} onPress={pickImage}>
                  <Text style={styles.buttonText}>Pick a Image</Text>
                </TouchableOpacity>
                {image && (
                  <Image
                    source={{ uri: image }}
                    style={{
                      width: 100,
                      height: 100,
                      marginTop: 5,
                      marginLeft: 20,
                    }}
                  />
                )}
              </View>

              <View style={{ width: "40%" }}>
                <TouchableOpacity
                  style={{
                    backgroundColor: "red",
                    borderRadius: 10,
                    padding: 10,
                  }}
                  onPress={() => Cancelbutton()}
                >
                  <Text
                    style={{
                      color: "white",
                      fontSize: 17,
                      fontWeight: "bold",
                      textAlign: "center",
                    }}
                  >
                    Cancel
                  </Text>
                </TouchableOpacity>
              </View>
            </View>
            {uploading && (
              <View style={styles.activityIndicatorContainer}>
                <ActivityIndicator size="large" color="#0000ff" />
              </View>
            )}
            <TouchableOpacity
              style={styles.button}
              onPress={() => uploadCategory()}
            >
              <Text style={styles.buttonText}>Upload</Text>
            </TouchableOpacity>
          </Stack>
        </View>
      </KeyboardAvoidingView>
    </>
  );
};

export default Post;

const styles = StyleSheet.create({
  buttonContainer: {
    width: "100%",
    // borderWidth: 1,
    display: "flex",
    flexDirection: "row",

    justifyContent: "space-between",
    // paddingLeft:20,
  },
  button: {
    backgroundColor: "#FFD662",
    borderRadius: 10,
    padding: 10,
    // height: 50,
    // margin: 20,
  },
  buttonText: {
    color: "white",
    fontSize: 17,
    fontWeight: "400",
    textAlign: "center",
  },
});
