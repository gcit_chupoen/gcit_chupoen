import {
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  ActivityIndicator,
  TextInput,
} from "react-native";
import { useNavigation } from "@react-navigation/native";
import { Ionicons } from "@expo/vector-icons";
import React, { useState, useEffect } from "react";

import AsyncStorage from "@react-native-async-storage/async-storage";
import { setDoc, doc, getDoc } from "firebase/firestore";
import { db } from "../../Firebase/firebase";
import DropDownPicker from "react-native-dropdown-picker";

const ProfileDetials = () => {
  const navigation = useNavigation();
  const [fullname, setFullname] = useState("");
  const [email, setEmail] = useState("");
  const [gender, setGender] = useState("");
  const [year, setYear] = useState("");
  const [semester, setSemester] = useState("");

  const [loading, setLoading] = useState(false);
  const [loggedInUser, setLoggedInUser] = useState(null);

  const [open, setOpen] = useState(false);
  const [openYear, setOpenYear] = useState(false);
  const [openSem, setOpenSem] = useState(false);

  const GenderOption = [
    { label: "Male", value: "Male" },
    { label: "Female", value: "Female" },
    { label: "Others", value: "Others" },
  ];

  const YearOption = [
    { label: "1st Year", value: "1st Year" },
    { label: "2nd Year", value: "2nd Year" },
    { label: "3rd Year", value: "3rd Year" },
    { label: "4th Year", value: "4th Year" },
  ];
  const SemOption = [
    { label: "First Semester", value: "First Semester" },
    { label: "Second Semester", value: "Second Semester" },
  ];

  const GenderhandleDropdownChange = (value) => {
    setGender(value);
  };

  const YearhandleDropdownChange = (value) => {
    setYear(value);
  };
  const SemhandleDropdownChange = (value) => {
    setSemester(value);
  };

  useEffect(() => {
    const ReadData = async () => {
      if (!loggedInUser) return;
      const docRef = doc(db, "users", loggedInUser);
      const docSnap = await getDoc(docRef);

      if (docSnap.exists()) {
        setFullname(docSnap.data().Name);
        setEmail(docSnap.data().Email);
        setGender(docSnap.data().Gender);
        setYear(docSnap.data().Year);
        setSemester(docSnap.data().Semester);
      }
    };

    ReadData();
  }, [loggedInUser]);

  useEffect(() => {
    const fetchData = async () => {
      setLoggedInUser(await getData());
    };

    fetchData();
  }, []);

  const saveData = async () => {
    setLoading(true);
    if (loggedInUser) {
      try {
        await setDoc(doc(db, "users", loggedInUser), {
          Name: fullname,
          Email: email,
          Gender: gender,
          Year: year,
          Semester: semester,
        });
        setLoading(false);
        alert("Profile updated successfully");

        // Open the drawer to the "ProfileScreen"
      } catch (error) {
        setLoading(false);
        alert(error.message);
      }
    }
    navigation.navigate("Profile");
  };
  const getData = async () => {
    const email = await AsyncStorage.getItem("Email");
    return email;
  };

  useEffect(() => {
    const fetchData = async () => {
      const email = await getData();
      if (email) {
        setLoggedInUser(email);
      }
    };

    fetchData();
  }, []);

  useEffect(() => {
    const fetchData = async () => {
      if (loggedInUser) {
        const docRef = doc(db, "users", loggedInUser);
        const docSnap = await getDoc(docRef);

        if (docSnap.exists()) {
          setFullname(docSnap.data().Name);
          setEmail(docSnap.data().Email);
          setGender(docSnap.data().Gender);
          setYear(docSnap.data().Year);
          setSemester(docSnap.data().Semester);
        }
      }
    };

    fetchData();
  }, [loggedInUser]);

  return (
    <>
      <View style={styles.mainContainer}>
        <View style={styles.ContentContainer}>
          <View style={styles.header}>
            <View style={styles.backButton}>
              <TouchableOpacity onPress={() => navigation.goBack()}>
                <Ionicons
                  name="chevron-back-circle-sharp"
                  size={30}
                  color="#F67F1E"
                />
              </TouchableOpacity>
            </View>
            <View style={styles.editDetails}>
              <Text style={styles.editDetailsText}>Edit Details</Text>
            </View>
          </View>
          <View style={styles.container}>
            <Text style={styles.label}>Full Name</Text>
            <TextInput
              placeholder="Enter your full name"
              value={fullname}
              onChangeText={setFullname}
              style={styles.textInput}
            />
          </View>
          <View style={styles.container}>
            <Text style={styles.label}>Email</Text>
            <TextInput
              placeholder="Enter your email"
              value={email}
              onChangeText={setEmail}
              style={styles.textInput}
            />
          </View>
          <View style={{ zIndex: open ? 1 : 0 }}>
            <Text style={styles.label}>Gender</Text>
            <DropDownPicker
              items={GenderOption}
              defaultValue={gender}
              placeholder="Select Gender"
              containerStyle={[styles.dropdownContainer, { height: 60 }]}
              style={styles.dropdown}
              dropDownStyle={styles.dropdown}
              open={open}
              setOpen={setOpen}
              value={gender}
              setValue={setGender}
              onChangeValue={GenderhandleDropdownChange}
            />
          </View>

          <View style={{ zIndex: openYear ? 1 : 0 }}>
            <Text style={styles.label}>Year of Study</Text>
            <DropDownPicker
              items={YearOption}
              defaultValue={year}
              placeholder="Select Year"
              containerStyle={[styles.dropdownContainer, { height: 60 }]}
              style={styles.dropdown}
              dropDownStyle={styles.dropdown}
              open={openYear}
              setOpen={setOpenYear}
              value={year}
              setValue={setYear}
              onChangeValue={YearhandleDropdownChange}
            />
          </View>
          <View style={{ zIndex: openSem ? 1 : 0 }}>
            <Text style={styles.label}>Current Semester</Text>
            <DropDownPicker
              items={SemOption}
              defaultValue={semester}
              placeholder="Select Semester"
              containerStyle={[styles.dropdownContainer, { height: 60 }]}
              style={styles.dropdown}
              dropDownStyle={styles.dropdown}
              open={openSem}
              setOpen={setOpenSem}
              value={semester}
              setValue={setSemester}
              onChangeValue={SemhandleDropdownChange}
            />
          </View>
          <TouchableOpacity
            style={{
              backgroundColor: "#FFD662",
              height: 50,
              margin: 20,
              justifyContent: "center",
              alignItems: "center",
              borderRadius: 10,
            }}
            onPress={() => saveData()}
          >
            <Text style={{ color: "white", fontSize: 18, fontWeight: "bold" }}>
              Save Data
            </Text>
          </TouchableOpacity>
          {loading && <ActivityIndicator />}
        </View>
      </View>
    </>
  );
};

export default ProfileDetials;

const styles = StyleSheet.create({
  mainContainer: {
    backgroundColor: "black",
    flex: 1,
    paddingTop: 40,
  },
  ContentContainer: {
    paddingHorizontal: 10,
    alignContent: "space-between",
    // borderWidth:2,
    borderColor: "red",
    backgroundColor: "white",
    height: "100%",
  },
  container: {
    marginBottom: 20,
  },
  label: {
    fontSize: 16,
    fontWeight: "bold",
    marginBottom: 5,
    color: "#333",
    marginLeft: 10,
  },

  textInput: {
    margin: 5,
    paddingVertical: 10,
    paddingHorizontal: 15,
    borderWidth: 1,
    backgroundColor: "#F7F7F7",
    borderColor: "#ddd",
    borderRadius: 10,
    fontSize: 16,
    color: "#333",
  },

  header: {
    flexDirection: "row",
    alignItems: "center",

    marginBottom: 20,
    marginTop: 20,
  },
  editDetails: {
    marginRight: 30,
  },
  editDetailsText: {
    fontSize: 20,
    fontWeight: "bold",
    color: "#F67F1E",
    marginLeft: 100,
  },
  dropdownContainer: {
    margin: 5,
    marginVertical: 10,
    marginRight: 30,
    width: "97%",
  },
  dropdown: {
    backgroundColor: "#fafafa",
    borderColor: "#ddd",
    height: 50,
    borderWidth: 1,
    borderRadius: 10,
    fontSize: 16,
    // marginRight:-20,
  },
});
