import React from "react";
import { useNavigation } from "@react-navigation/native";
import { db } from "../../Firebase/firebase";
import AsyncStorage from "@react-native-async-storage/async-storage";
import {
  View,
  Text,
  StyleSheet,
  Image,
  TouchableOpacity,
  ImageBackground,
} from "react-native";
import Icon from "@expo/vector-icons/MaterialCommunityIcons";
import { doc, getDoc } from "firebase/firestore";
import { useEffect, useState } from "react";
import { Ionicons } from "@expo/vector-icons";

export default function ProfilePage() {
  const navigation = useNavigation();
  const [loggedInUser, setLoggedInUser] = useState(null);

  const [data, setData] = useState([]);

  const getData = async () => {
    const email = await AsyncStorage.getItem("Email");
    return email;
  };

  useEffect(() => {
    const fetchData = async () => {
      setLoggedInUser(await getData());
    };
    fetchData();
  }, []);

  useEffect(() => {
    const ReadData = async () => {
      const docRef = doc(db, "users", loggedInUser);
      const docSnap = await getDoc(docRef);
      const itemArray = [];

      if (docSnap.exists()) {
        itemArray.push({
          name: docSnap.data().Name,
          email: docSnap.data().Email,
          gender: docSnap.data().Gender,
          year: docSnap.data().Year,
          semester: docSnap.data().Semester,
        });
        setData(itemArray);
      }
    };

    ReadData();
  }, [loggedInUser, data]);

  function EditDetails() {
    navigation.navigate("ProfileDetials");
  }
  // console.log(data);
  return (
    <View style={styles.container}>
      <View style={styles.bacgroundImageContainer}>
        <ImageBackground
          borderRadius={10}
          style={styles.image}
          source={require("../../assets/image/pattern.png")}
        >
          <View style={styles.BackButton}>
            <TouchableOpacity onPress={() => navigation.goBack()}>
              <Ionicons
                name="chevron-back-circle-sharp"
                size={35}
                color="#F67F1E"
              />
            </TouchableOpacity>
          </View>
          <Image
            style={styles.image}
            source={require("../../assets/image/logo.png")}
          />
          <View style={styles.topContainer}>
            <View style={styles.initialContainer}>
              <Text style={styles.initialText}>
                {data[0]?.name ? data[0]?.name.charAt(0).toUpperCase() : ""}
              </Text>
            </View>

            <Text style={styles.profileName}>{data[0]?.name}</Text>
            {/* <Text style={styles.memberSince}>Member since July 23, 2020</Text> */}

            <View style={styles.buttonsContainer}>
              <TouchableOpacity>
                <View style={styles.button}>
                  <Icon name="file-image-plus" size={22} color={"#F67F1E"} />
                  <Text style={styles.buttonText}>Change Photo</Text>
                </View>
              </TouchableOpacity>
              <TouchableOpacity onPress={EditDetails}>
                <View style={styles.button}>
                  <Icon
                    name="square-edit-outline"
                    size={22}
                    color={"#F67F1E"}
                  />
                  <Text style={styles.buttonText}>Edit Profile</Text>
                </View>
              </TouchableOpacity>
            </View>
          </View>
        </ImageBackground>
      </View>

      <View style={styles.bottomContainer}>
        <View style={styles.detailsContainer}>
          <View style={{ alignItems: "center" }}>
            <Text style={styles.heading}>Your Details</Text>
          </View>
          <View style={styles.detailRow}>
            <View style={{ paddingRight: 20 }}>
              <Text>
                <Icon name="gender-male-female" size={30} color={"#F67F1E"} />
              </Text>
            </View>
            <View>
              <View>
                <Text style={styles.detailLabel}>Gender:</Text>
              </View>
              <View>
                <Text style={styles.detailValue}>{data[0]?.gender}</Text>
              </View>
            </View>
          </View>
          <View style={styles.detailRow}>
            <View style={{ paddingRight: 20 }}>
              <Text>
                <Icon name="school" size={27} color={"#F67F1E"} />
              </Text>
            </View>
            <View>
              <View>
                <Text style={styles.detailLabel}>Year:</Text>
              </View>
              <View>
                <Text style={styles.detailValue}>
                  {data[0]?.year} {data[0]?.semester}
                </Text>
              </View>
            </View>
          </View>
          <View style={styles.detailRow}>
            <View style={{ paddingRight: 20 }}>
              <Text>
                <Icon name="email" size={27} color={"#F67F1E"} />
              </Text>
            </View>
            <View>
              <View>
                <Text style={styles.detailLabel}>Email:</Text>
              </View>
              <View>
                <Text style={styles.detailValue}>{data[0]?.email}</Text>
              </View>
            </View>
          </View>
        </View>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#FFFFFF",
  },
  topContainer: {
    flex: 1,
    alignItems: "center",

    marginTop: -30,
  },
  logo: {
    width: 100,
    height: 100,
    marginTop: 10,
  },
  profileIcon: {
    marginTop: 20,
  },
  profileName: {
    fontSize: 18,
    marginTop: 10,
    fontWeight: "400",
    color: "black",
  },

  buttonsContainer: {
    flexDirection: "row",
  },
  button: {
    flexDirection: "row",
    borderRadius: 5,
    paddingVertical: 8,
    paddingHorizontal: 5,
    marginHorizontal: 10,
  },
  buttonText: {
    color: "#F67F1E",
    fontSize: 14,
    paddingLeft: 10,
  },
  bottomContainer: {
    borderTopColor: "#DBDBDB",
    borderTopWidth: 1,
    height: 300,
    paddingTop: 30,
    paddingLeft: 10,
    paddingRight: 10,
    marginTop: -70,
  },
  heading: {
    fontSize: 20,
    fontWeight: "400",
    marginBottom: 10,
    color: "black",
  },
  detailRow: {
    flexDirection: "row",
    paddingVertical: 10,
  },
  detailLabel: {
    fontWeight: "400",
    color: "black",
  },
  detailValue: {
    fontSize: 12,
    color: "#A2AEBB",
  },

  detailsContainer: {
    borderWidth: 1,
    borderColor: "#DBDBDB",
    borderRadius: 20,
    padding: 20,
    width: "100%",
  },
  image: {
    width: "100%",
    height: "70%",
  },
  bacgroundImageContainer: {
    width: "100%",
    height: "60%",
  },
  BackButton: {
    width: 40,
    marginLeft: 20,
    marginTop: 20,
  },
  initialContainer: {
    width: 60,
    height: 60,
    borderRadius: 35,
    backgroundColor: "#f0f0f0",
    justifyContent: "center",
    alignItems: "center",
    marginLeft: 10,
    marginTop:20,
  },
  initialText: {
   
    fontSize: 30,
    fontWeight: "bold",
    color: "#333",
  },
});
