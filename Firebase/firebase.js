
import firebase from 'firebase/compat/app';
import 'firebase/compat/auth';
import { getFirestore} from 'firebase/firestore';
import  {getStorage} from 'firebase/storage'

const firebaseConfig = {
  apiKey: "AIzaSyCrZ73HFgbPoMC9EP1TupNNZ5ac9q_MV9g",
  authDomain: "chupoen-3d431.firebaseapp.com",
  databaseURL: "https://chupoen-3d431-default-rtdb.firebaseio.com",
  projectId: "chupoen-3d431",
  storageBucket: "chupoen-3d431.appspot.com",
  messagingSenderId: "1035506323139",
  appId: "1:1035506323139:web:be9ede8c07a40f53fb9c76",
  measurementId: "G-4TPKH2LMT3",
};

// Initialize Firebase

if (!firebase.apps.length) {
  firebase.initializeApp(firebaseConfig);
}
export{firebase};

const app= firebase.initializeApp(firebaseConfig);

export const auth = firebase.auth();
export const db=getFirestore(app);
export const storage=getStorage(app);




